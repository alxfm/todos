import io from 'socket.io-client'
import feathers from '@feathersjs/feathers'
import socketio from '@feathersjs/socketio-client'

const socket = io('http://173.249.15.201:3231/')
const client = feathers()
  .configure(socketio(socket))

export { socket, client }
