// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import ElementUI from 'element-ui';
import locale from 'element-ui/lib/locale/lang/en'
import 'element-ui/lib/theme-chalk/index.css';
import vueFeathers from 'vue-feathers'
import { client } from './feathers'
import App from './App'
import moment from 'moment';

Vue.config.productionTip = false;

Vue.use (ElementUI, { locale });
Vue.use(vueFeathers, client);
Vue.prototype.moment = moment

/* eslint-disable no-new */
new Vue({
  el: '#app',
  components: { App },
  template: '<App/>'
})
